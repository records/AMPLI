---
header-includes:
  - \usepackage{booktabs}
  - \usepackage{longtable}
  - \usepackage{array}
  - \usepackage{multirow}
  - \usepackage{wrapfig}
  - \usepackage{float}
  - \usepackage{colortbl}
  - \usepackage{pdflscape}
  - \usepackage{tabu}
  - \usepackage{threeparttable}
  - \usepackage{threeparttablex}
  - \usepackage[normalem]{ulem}
  - \usepackage[utf8]{inputenc}
  - \usepackage{makecell}
  - \usepackage{xcolor}
geometry: "left=1.5cm,right=1.5cm,top=1cm,bottom=1.5cm"
output:
  pdf_document:
    latex_engine: xelatex
    keep_tex: false
documentclass: article
classoption: a4paper
params:
  report_data: NA
editor_options: 
  chunk_output_type: console
---

---
title: `r paste0("Rapport statique (", params$report_data$user_hash, ")")`
---

```{r, echo = FALSE}
library(tidyverse)
library(knitr)
library(kableExtra)
library(patchwork)
extrafont::loadfonts()
#options(tinytex.verbose = TRUE)
```

## Information socio-démographiques

### Informations de base (réponses de l'enquête)

```{r, echo = FALSE}
sociodemo_table <- params$report_data$report_sociodemo %>%
  filter(!Var %in% c("Lat", "Long"))
kableExtra::kable(sociodemo_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = "striped", full_width = TRUE) %>%
  column_spec(1, width = "4cm", border_right=TRUE) %>%
  column_spec(2, width = "10cm")
```

## Pratiques d'écoute

### Récapitulatif des pratiques d'écoute

```{r, echo = FALSE}
pratiques_table <- params$report_data$report_pratiques %>%
  mutate(Var = str_replace_all(Var, "<br/>", " ")) %>%
  mutate(Réponse = str_replace_all(Rep, "<br/>", " ")) %>%
  select(-Rep)
kableExtra::kable(pratiques_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = "striped", full_width = TRUE) %>%
  column_spec(1, width = "4cm", border_right = TRUE) %>%
  column_spec(2, width = "10cm")
```

### Temporalités du streaming

```{r, echo = FALSE, fig.show='hold', fig.width=3.5, fig.height=2.5}
plot1 <- params$report_data$report_pratiques_quotidien + theme_report()
plot1$layers[[1]]$aes_params$colour <- NULL
plot1
params$report_data$report_pratiques_hebdo + theme_report()
```

\newpage

## Contextes d'écoute

### Type de recommandation

```{r, echo = FALSE}
reco_table <- params$report_data$report_contexte_reco
kableExtra::kable(reco_table %>% select(-MainContext),
                  format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = c("striped", "scale_down"), full_width = TRUE) %>%
  pack_rows(index = table(fct_inorder(reco_table$MainContext))) %>%
  column_spec(1,  border_right = TRUE) %>%
  column_spec(2,  border_right = TRUE)
  
```

### Temporalités des écoutes par type de recommandation

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
params$report_data$report_contextes_plot_reco & theme_report()
```

### Temporalités des écoutes par plateforme

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
params$report_data$report_contextes_plot_device & theme_report()
```

\newpage

## Contextes spatiaux

### Clusters spatiaux

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
params$report_data$report_spatial_map
```

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
summary_table <- params$report_data$report_spatial_summary
kableExtra::kable(summary_table,
                  format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = c("striped", "scale_down"), full_width = TRUE) %>%
  column_spec(1,  border_right = TRUE) %>%
  column_spec(2,  border_right = TRUE)
```

### Temporalités des écoutes par cluster

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
params$report_data$report_spatial_temporal & theme_report()
```

### Genres écoutés par cluster

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
params$report_data$report_spatial_genres & theme_report()
```

\newpage

## Genres écoutés

### Récapitulatif des genres

```{r, echo = FALSE}
genres_table <- params$report_data$report_genres
kableExtra::kable(genres_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = c("striped", "scale_down"), full_width = TRUE) %>%
  column_spec(1, border_right = TRUE) %>%
  column_spec(2, border_right = TRUE)
  
```

### Genres et tags principaux

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
params$report_data$report_tags_plot +
  theme_report() +
    theme(
    strip.placement = "outside",
    strip.background = element_blank(),
    strip.text = element_text(colour = "black", face = "bold"),
    panel.grid.major.y = element_blank()
  )
```

(5 genres les plus écoutés, et pour chacun, les 5 tags les plus écoutés)

```{r, echo = FALSE}
# tags_data <- tags_createTable(user_songs)
tags_data <- params$report_data$report_tags

tags_table_data <- tags_data %>%
  mutate(across(ends_with("_all"), .fns = ~str_replace_all(.x, "%", replacement = "\\\\%"))) %>%
  mutate(across(ends_with("_all"), .fns = ~str_replace(.x, "<br/>", replacement = "\n"))) %>%
  mutate(across(ends_with("_all"), .fns = ~linebreak(.x))) %>%
  mutate(across(ends_with("_Tag"), .fns = ~str_replace_all(.x, "_", replacement = "-"))) %>%
  mutate(across(ends_with("_Tag"), .fns = ~str_replace_all(.x, "&", replacement = " and ")))


tags_genres <- colnames(tags_table_data) %>%
    str_replace_all("_Tag$", "") %>%
    str_replace_all("_all$", "") %>%
    unique()

kableExtra::kbl(tags_table_data, col.names = NULL, format = "latex", escape = FALSE,  booktabs = TRUE, linesep = "") %>%
kable_styling(latex_options = c("striped", "scale_down"), full_width = TRUE) %>%
  add_header_above(
    tibble(genre = tags_genres,
           colspan = rep(x = 2, times = 5))
  ) %>%
  column_spec(seq(1, 5*2, 2), bold = TRUE) %>%
  column_spec(seq(2, 5*2, 2), border_right = TRUE)
  
```

### Genres et temporalité

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
params$report_data$report_ranks_genres + theme_report()
```

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
patchwork_plot <- params$report_data$report_genres_plot & theme_report() & theme(
    axis.text.x = element_text(size = 8),
    strip.text.y.left = element_text(size = 8,
                                     margin = margin(t = 0, r = 0, b = 0, l = 0),
                                     angle = 0, colour = "black",
                                     hjust = 1),
    strip.background = element_blank(),
    axis.text.y = element_blank(),
    axis.ticks.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank()
  )
patchwork_plot[[2]] <- patchwork_plot[[2]] + theme(
   strip.text.y.left = element_blank()
)
patchwork_plot[[3]] <- patchwork_plot[[3]] + theme(
   strip.text.y.left = element_blank()
)

patchwork_plot
```

# Organisation de la musique

Manque des données sur les playlists :

-   nombre de playlists
-   taille des playlists <!--
    ### Nombre de playlists personnelles ; taille des playlists
    ### Nombre de favoris. 
    -->

### Nombre et type de favoris

```{r, echo = FALSE}
 organisation_table <- params$report_data$report_organisation_table
  
  nb_rows_favs <- nrow(organisation_table)
  
  kableExtra::kable(organisation_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
    kable_styling(latex_options = c("striped"), full_width = TRUE) %>%
    column_spec(1, border_right = TRUE, width = "3cm") %>%
    column_spec(2, width = "2cm") %>%
    row_spec(row = nb_rows_favs, bold = TRUE)
```

### Fréquence d'usage de la mise en favori

```{r, echo = FALSE, fig.show='hold', fig.width=8, fig.height=3}
this_plot <- params$report_data$report_organisation_frequence_plot
this_plot$data <- this_plot$data %>%
  mutate(type = ifelse(type %in% c("Ajout de favoris", "Streams"), yes = type, no = "Click Loved"))
this_plot + theme_report()
```

# Intermédiation et sociabilités

Pas faisable avec les données actuelles :

-   Abonnements à d'autres usagers Deezer/à des playlists non produites
    par la plateforme. Lesquelles?
-   Nombre d'utilisateurs déclarés sur le compte.
-   Compte familial ou personnel?
-   Si possible: stat descriptive sur la présence / le nombre d'"amis"
    sur Deezer (si l'information est disponible). <!--
    # Abonnements à d’autres usagers Deezer/à des playlists non produites par la plateforme. Lesquelles?
    # Nombre d’utilisateurs déclarés sur le compte.
    # Compte familial ou personnel?
    # Si possible: stat descriptive sur la présence / le nombre d’“amis” sur Deezer (si l’information est disponible).
    -->

# Goûts musicaux

```{=html}
<!--
# Artistes et/ou tags les plus représentatifs du profil + de chaque année
# Grappes d’artistes écoutés
# Chansons écoutées avec le plus de régularité
-->
```
### Artistes les plus écoutés

```{r, echo = FALSE}
 gouts_artistes_table <- params$report_data$report_gouts_artistes_table

  artistes_favs <- gouts_artistes_table %>%
    mutate(row_id = row_number()) %>%
    filter(fav_artist) %>%
    pull(row_id)
  
  kableExtra::kable(gouts_artistes_table %>% select(-fav_artist), format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
    kable_styling(latex_options = c("striped"), full_width = TRUE) %>%
    column_spec(1, border_right = TRUE, width = "1.5cm") %>%
    column_spec(c(3,4), border_right = TRUE, width = "2.5cm") %>%
    column_spec(2, border_right = TRUE) %>%
    row_spec(row = artistes_favs, bold = TRUE) %>%
    footnote(general_title = "N.B.", general = "Les artistes en gras sont présents dans les favoris de l'utilisateur", 
             footnote_as_chunk = TRUE, title_format = "bold")
```

### Chansons les plus écoutées

```{r, echo = FALSE, eval = TRUE}
 gouts_songs_table <- params$report_data$report_gouts_songs_table
  
  latex_table <- gouts_songs_table %>%
    select(-fav_song, -fav_artist)

  kableExtra::kable(latex_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
    kable_styling(latex_options = c("striped"), full_width = TRUE) %>%
    column_spec(c(1,4), border_right = TRUE, width = "1.5cm") %>%
    column_spec(c(5,6), border_right = TRUE, width = "2.25cm") %>%
    column_spec(2, bold = pull(gouts_songs_table, fav_song), width= "3.5cm") %>%
    column_spec(3, bold = pull(gouts_songs_table, fav_artist), width = "3.5cm") %>%
    footnote(general_title = "N.B.", general = "Les chansons et/ou artistes en gras sont dans les favoris de l'utilisateur", 
             footnote_as_chunk = TRUE, title_format = "bold")
```

# Dégoûts musicaux

```{=html}
<!--
# Top des chansons/artistes/tags “bannis” et skippés
-->
```
### Chansons bannies les plus rapidement

```{r, echo = FALSE, eval = TRUE}
fastest_banned_table <- params$report_data$report_degouts_banned_table

kableExtra::kable(fastest_banned_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = c("striped"), full_width = TRUE) %>%
  column_spec(c(1,3), border_right = TRUE, width = "1.5cm") %>%
  column_spec(2, border_right = TRUE, width = "5cm") %>%
  column_spec(4, border_right = TRUE, width = "3cm") %>%
  column_spec(column = 5, border_right = TRUE, width = "2cm") %>%
  column_spec(column = 6, border_right = TRUE, width = "1.5cm")
```

### Chansons les plus "skippées" (parmi celles écoutées plus de 10 fois)

```{r, echo = FALSE, eval = TRUE}
most_skipped_table <- params$report_data$report_degouts_skipped_table %>%
  mutate(skip_ratio = round(skip_ratio, digits = 3)) %>%
  mutate(skip_ratio = scales::percent(skip_ratio))

kableExtra::kable(most_skipped_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = c("striped"), full_width = TRUE) %>%
  column_spec(c(1,3), border_right = TRUE, width = "1.5cm") %>%
  column_spec(c(2,4), border_right = TRUE) %>%
  column_spec(column = 5:7, border_right = TRUE, width = "1.5cm")
```

### Artistes "centraux" jamais écoutés

```{r, echo = FALSE, eval = TRUE}
top_degrees_table <- params$report_data$report_degouts_graph_table %>%
  mutate(ratio_degree = round(ratio_degree, digits = 3)) %>%
  mutate(ratio_degree = scales::percent(ratio_degree))

kableExtra::kable(top_degrees_table, format = "latex", escape = TRUE,  booktabs = TRUE, linesep = "") %>%
  kable_styling(latex_options = c("striped"), full_width = TRUE) %>%
  column_spec(1, border_right = TRUE, width = "1.5cm") %>%
  column_spec(2, border_right = TRUE) %>%
  column_spec(column = 3:5, border_right = TRUE, width = "2cm")
```
