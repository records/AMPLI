function playTrack(track_id) {
	var api_url = "https://api.deezer.com/track/" + track_id + "/?output=jsonp";
	$.ajax({
		url: api_url,
		jsonp: "callback",
		dataType: "jsonp",
		data: {
			format: "json"
		},
		success: function(response) {
			my_result = response;
			console.log(response);
			var album_image = $("#img_album");
			album_image[0].src = response.album.cover_big;
			var audioplayer = $("#music-player")[0];
			audioplayer.setAttribute("src", response.preview);
			$("#artist_name")[0].innerHTML = response.artist.name;
			$("#track_title")[0].innerHTML = response.title;
			$("#album_title")[0].innerHTML = response.album.title;
			
			//var no_preview_text = $('#no-preview-text')[0];
			if (response.preview != "") {
			  //no_preview_text.innerHTML = "";
		  	album_image.removeClass("paused");
			  album_image.addClass("playing");
		  	audioplayer.load();
			  audioplayer.play();
			} else {
  			album_image.removeClass("playing");
  			album_image.addClass("paused");
			  //no_preview_text.innerHTML = "No audio available";
			}

		}
	});
}

function playArtistTopSong(artist_id) {
	var api_url = "https://api.deezer.com/artist/" + artist_id + "/top?output=jsonp";
	$.ajax({
		url: api_url,
		jsonp: "callback",
		dataType: "jsonp",
		data: {
			format: "json"
		},
		success: function(response) {
			my_result = response.data;
			console.log(my_result);
			let nb_results = my_result.length;
			let random_number = Math.floor(Math.random() * nb_results) + 1;
			random_element = my_result[random_number];
			var album_image = $("#img_album");
			album_image[0].src = random_element.album.cover_big;
			var audioplayer = $("#music-player")[0];
			audioplayer.setAttribute("src", random_element.preview);
			$("#artist_name")[0].innerHTML = random_element.artist.name;
			$("#track_title")[0].innerHTML = random_element.title;
			$("#album_title")[0].innerHTML = random_element.album.title;
			
			//var no_preview_text = $('#no-preview-text')[0];
			if (random_element.preview != ""){
  			album_image.removeClass("paused");
  			album_image.addClass("playing");
  			audioplayer.load();
			  audioplayer.play();
			  //no_preview_text.innerHTML = "";
			} else {
  			album_image.removeClass("playing");
  			album_image.addClass("paused");
			  //no_preview_text.innerHTML = "No audio available";
			}
		}
	});
}

function togglePlayPause() {
	var audioplayer = $("#music-player")[0];
	var album_image = $("#img_album");
	if (audioplayer.paused) {
		album_image.removeClass("paused");
		album_image.addClass("playing");
		audioplayer.play();
	} else {
		album_image.removeClass("playing");
		album_image.addClass("paused");
		audioplayer.pause();
	}
}

function setVolume(value) {
	var audioplayer = $("#music-player")[0];
	audioplayer.volume = value / 100;
}