function play_song(id, type){
  var users = $("#user_id");
  var this_user = users.val();
  var shorter_user_hash = this_user.substring(0, 8);
  var this_message = shorter_user_hash.concat("_player_");
  
  if (type == 'track'){
    var abc = playTrack(id);
    gtag('event', this_message.concat("song"));
    //console.log(this_message.concat("song"));
  } else if (type == 'artist'){
     var abc = playArtistTopSong(id);
     gtag('event', this_message.concat("artist"));
     //console.log(this_message.concat("artist"));
  } else 
  {}
};

Shiny.addCustomMessageHandler('play_this_song', function(id) {
  play_song(id, 'track');
});
Shiny.addCustomMessageHandler('play_this_artist', function(id){
  play_song(id, 'artist');  
});


/* Table pop-over message */
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});


/* Google Analytics events */

function sendEvent(id) {
  var users = $("#user_id");
  var this_user = users.val();
  var shorter_user_hash = this_user.substring(0, 8);
  var clean_event = id.replace('header_acco_', '').replace('header', 'survey').replace('-accordion', '');
  var eventName = shorter_user_hash.concat("_", clean_event);
  gtag('event', eventName);
  //gtag('event', id, {'event_category': this_user});
  //console.log(eventName);
};

$(document).ready(function() {
  
  $("div.sidebar a.nav-link").attr("onClick", "sendEvent(this.id.substr(4))");
  $(".card-header").attr("onClick", "sendEvent(this.id)");
  $("#download_report").attr("onClick", "sendEvent('report_download')");
});


/* Plot maximization events */
$('#dayPlot').ready(function(){
      //debugger;
      //console.log("dayPlot ready");
  $('button[data-card-widget="maximize"]').on('click', function(){
    //console.log(this);
    //debugger;
    var isMaximized = $('html').hasClass('maximized-card');
    var thisCard = $(this).parent().parent().parent();
    var thisPlot = $(thisCard).children('.card-body').children('div');
    if (!$(thisPlot).hasClass('html-widget')){
      // For getting the plots with tabset (graphs and tables)
       var thisPlot = $(thisCard).children('.card-body').children('.tab-content').children('.tab-pane .active').children('div');
       $(thisCard).children('.card-body').children('.tab-content').css('height', '100%');
       $(thisCard).children('.card-body').children('.tab-content').children('.tab-pane .active').css('height', '100%');
    };
    var is_collapsed = $(thisCard).hasClass('collapsed-card');
    if (is_collapsed){
      // do nothing
    } else {
      // not collapsed, change height
      if (!isMaximized){
       // Si on maximise
       var oldHeight = $(thisPlot).height();
       $(thisPlot).attr('original_height', oldHeight);
       $(thisPlot).css('height', '100%');
       $(thisPlot).trigger('resize');
    } else {
       // Si on est déjà en mode maximisé
       var oldHeight = $(thisPlot).attr('original_height');
       $(thisPlot).css('height', oldHeight);
       $(thisPlot).trigger('resize');
    };
    };
    
  });
});


