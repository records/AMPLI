# ==== day plot ====
output$dayPlot <- renderEcharts4r({
  if (debug_whereami) whereami::cat_where(where = whereami::whereami())
  req(user_data$user_id)

  user_streams <- prepare_data_dayPlot(user_data$streams)
  e_charts(user_streams, heure) %>%
    e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Global") %>%
    e_x_axis(index = 1, position = "bottom", splitNumber = 6) %>%
    e_format_x_axis(index = 1, suffix = "h") %>%
    e_x_axis(index = 0, show = FALSE) %>%
    e_y_axis(formatter = e_axis_formatter(style = "percent")) %>%
    e_grid(height = "80%", width= "80%", left = "15%", right = "5%", bottom = "10%", top = "10%") %>% 
    e_theme(echarts_theme) %>%
    e_toolbox() %>%
    e_toolbox_feature(
      feature = "brush", type = list("lineX", "clear")
    ) %>%
    e_brush(x_index = 1,
            throttleType = "debounce",
            throttleDelay = 300
    )
})

# ==== week plot ====
output$weekPlot <- renderEcharts4r({
  if (debug_whereami) whereami::cat_where(where = whereami::whereami())
  req(user_data$user_id)
  
  user_streams <- prepare_data_weekPlot(user_data$streams)
  e_charts(user_streams, njour) %>%
    e_bar(rel_time, x_index = 1,areaStyle = list(opacity = 0.5), name = "Global") %>%
    e_grid(height = "80%", width= "80%", left = "15%", right = "5%", bottom = "10%", top = "10%") %>% 
    e_x_axis(index = 1, show = FALSE) %>%
    e_y_axis(formatter = e_axis_formatter(style = "percent")) %>%
    e_theme(echarts_theme) %>%
    e_toolbox() %>%
    e_toolbox_feature(
      feature = "brush", type = list("lineX", "clear")
    ) %>%
    e_brush(x_index = 1,
            throttleType = "debounce",
            throttleDelay = 300
    )
})


# ==== month plot ====
output$monthPlot <- renderEcharts4r({
  if (debug_whereami) whereami::cat_where(where = whereami::whereami())
  req(user_data$user_id)
  
  user_streams <- prepare_data_monthPlot(user_data$streams)
  e_charts(user_streams, mois) %>%
    e_bar(rel_time, x_index = 1,areaStyle = list(opacity = 0.5), name = "Global") %>%
    e_x_axis(index = 1, position = "bottom") %>%
    e_y_axis(formatter = e_axis_formatter(style = "percent")) %>%
    e_grid(height = "80%", width= "80%", left = "15%", right = "5%", bottom = "10%", top = "10%") %>% 
    e_theme(echarts_theme) %>%
    e_toolbox() %>%
    e_toolbox_feature(
      feature = "brush", type = list("lineX", "clear")
    ) %>%
    e_brush(x_index = 1,
            throttleType = "debounce",
            throttleDelay = 300
    )
})

# ==== year plot ====
output$yearPlot <- renderEcharts4r({
  if (debug_whereami) whereami::cat_where(where = whereami::whereami())
  req(user_data$user_id)
  user_streams <- prepare_data_yearPlot(user_data$streams)
  e_charts(user_streams, annee) %>%
    e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Global") %>%
    e_x_axis(index = 1, position = "bottom") %>%
    e_y_axis(formatter = e_axis_formatter(style = "percent")) %>%
    e_grid(height = "80%", width= "80%", left = "15%", right = "5%", bottom = "10%", top = "10%") %>% 
    e_theme(echarts_theme) %>%
    e_toolbox() %>%
    e_toolbox_feature(
      feature = "brush", type = list("lineX", "clear")
    ) %>%
    e_brush(x_index = 1,
            throttleType = "debounce",
            throttleDelay = 300
    )
})

# ==== Update plots on spatial filter ====
observe({
  if (debug_whereami) whereami::cat_where(where = whereami::whereami())
  if (length(spatial_filtred_data$streams) > 1){
    echarts4rProxy("dayPlot") %>% e_remove_serie(serie_name = "Spatial")
    echarts4rProxy("weekPlot") %>% e_remove_serie(serie_name = "Spatial")
    echarts4rProxy("monthPlot") %>% e_remove_serie(serie_name = "Spatial")
    echarts4rProxy("yearPlot") %>% e_remove_serie(serie_name = "Spatial")
    
    #* day plot ====
    spatial_streams_heure <- prepare_data_dayPlot(spatial_filtred_data$streams)
    echarts4rProxy("dayPlot", data = spatial_streams_heure, x = heure) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Spatial") %>%
      e_execute()
    #* week plot ====
    spatial_streams_jour <- prepare_data_weekPlot(spatial_filtred_data$streams)
    echarts4rProxy("weekPlot", data = spatial_streams_jour, x = njour) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Spatial") %>%
      e_execute()
    #* month plot ====
    spatial_streams_month <- prepare_data_monthPlot(spatial_filtred_data$streams)
    echarts4rProxy("monthPlot", data = spatial_streams_month, x = mois) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Spatial") %>%
      e_execute()
    #* year plot ====
    spatial_streams_year <- prepare_data_yearPlot(spatial_filtred_data$streams)
    echarts4rProxy("yearPlot", data = spatial_streams_year, x = annee) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Spatial") %>%
      e_execute()
  } else {
    echarts4rProxy("dayPlot") %>% e_remove_serie(serie_name = "Spatial")
    echarts4rProxy("weekPlot") %>% e_remove_serie(serie_name = "Spatial")
    echarts4rProxy("monthPlot") %>% e_remove_serie(serie_name = "Spatial")
    echarts4rProxy("yearPlot") %>% e_remove_serie(serie_name = "Spatial")
  }
})

observeEvent(input$reset_spatial,{
  if (debug_whereami) whereami::cat_where(where = whereami::whereami())
  echarts4rProxy("dayPlot") %>% e_remove_serie(serie_name = "Spatial")
  echarts4rProxy("weekPlot") %>% e_remove_serie(serie_name = "Spatial")
  echarts4rProxy("monthPlot") %>% e_remove_serie(serie_name = "Spatial")
  echarts4rProxy("yearPlot") %>% e_remove_serie(serie_name = "Spatial")
})


# ==== Update plots on tag filter ====
observe({
  if (debug_whereami) whereami::cat_where(where = whereami::whereami())
  if (length(tag_filtred_data$streams) > 1){
    echarts4rProxy("dayPlot") %>% e_remove_serie(serie_name = "Tags")
    echarts4rProxy("weekPlot") %>% e_remove_serie(serie_name = "Tags")
    echarts4rProxy("monthPlot") %>% e_remove_serie(serie_name = "Tags")
    echarts4rProxy("yearPlot") %>% e_remove_serie(serie_name = "Tags")
    
    #* day plot ====
    tags_streams_heure <- prepare_data_dayPlot(tag_filtred_data$streams)
    echarts4rProxy("dayPlot", data = tags_streams_heure, x = heure) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Tags") %>%
      e_execute()
    #* week plot ====
    tags_streams_jour <- prepare_data_weekPlot(tag_filtred_data$streams)
    echarts4rProxy("weekPlot", data = tags_streams_jour, x = njour) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Tags") %>%
      e_execute()
    #* month plot ====
    tags_streams_month <- prepare_data_monthPlot(tag_filtred_data$streams)
    echarts4rProxy("monthPlot", data = tags_streams_month, x = mois) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Tags") %>%
      e_execute()
    #* year plot ====
    tags_streams_year <- prepare_data_yearPlot(tag_filtred_data$streams)
    echarts4rProxy("yearPlot", data = tags_streams_year, x = annee) %>%  # create a proxy
      e_bar(rel_time, x_index = 1, areaStyle = list(opacity = 0.5), name = "Tags") %>%
      e_execute()
  } else {
    echarts4rProxy("dayPlot") %>% e_remove_serie(serie_name = "Tags")
    echarts4rProxy("weekPlot") %>% e_remove_serie(serie_name = "Tags")
    echarts4rProxy("monthPlot") %>% e_remove_serie(serie_name = "Tags")
    echarts4rProxy("yearPlot") %>% e_remove_serie(serie_name = "Tags")
  }
})