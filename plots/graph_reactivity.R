# ==== Reactive data ====
graph_filtred_data <- reactiveValues(
  streams = NULL, songs = NULL, tags = NULL
)

# ==== Play songs on click ====

# * Base graph -----
observeEvent(input$graph_clicked_data,{
  clicked_artist <- input$graph_clicked_data$value
  session$sendCustomMessage("play_this_artist", clicked_artist)
})

# * Time graph -----
observeEvent(input$time_graph_clicked_data,{
  clicked_artist <- input$time_graph_clicked_data$value
  session$sendCustomMessage("play_this_artist", clicked_artist)
})

# * Spatial graph -----
observeEvent(input$space_graph_clicked_data,{
  clicked_artist <- input$space_graph_clicked_data$value
  session$sendCustomMessage("play_this_artist", clicked_artist)
})

# * Tags graph -----
observeEvent(input$tag_graph_clicked_data,{
  clicked_artist <- input$tag_graph_clicked_data$value
  session$sendCustomMessage("play_this_artist", clicked_artist)
})